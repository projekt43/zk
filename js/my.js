
// Funkce pro aktualizaci šířky a posunutí prvku s třídou "swiper"
function aktualizovatSirkuAPosunuti() {
  const sirkaZarizeni = window.innerWidth;
  const swiperElement = document.querySelector(".swiper");
  const posunuti = (sirkaZarizeni - swiperElement.offsetWidth) / 2;
  swiperElement.style.width = sirkaZarizeni + 'px';
  swiperElement.style.right = posunuti + 'px';
}

// Zavolání funkce při načtení stránky a po každé změně velikosti okna prohlížeče
window.addEventListener("load", aktualizovatSirkuAPosunuti);
window.addEventListener("resize", aktualizovatSirkuAPosunuti);
/*
// získání výšky okna
const windowHeight = window.innerHeight;
// získání prvku s třídou header
const header = document.querySelector('.header');
// získání výšky prvku header
const headerHeight = header.offsetHeight;
// výpočet výšky body (odečtení výšky headeru od výšky okna)
const bodyHeight = windowHeight - headerHeight;
// nastavení výšky prvku body na výslednou hodnotu
document.querySelector('body').style.height = bodyHeight + 'px';

const section3width = document.querySelector('.section3');
console.log(section3)
*/

$(document).ready(function(){

const aplikace_slider = new Swiper('.aplikace_slider', {
  
  loop: true,
  pagination: {
    el: '.swiper-pagination',
  },
  breakpoints: {
    0: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
  },
});


$('.controls .next').click(function(){
  aplikace_slider.slideNext();
})

$('.controls .prev').click(function(){
  aplikace_slider.slidePrev();
})

})















































/*
$(document).ready(function() {
    // cache the jQuery object for the element with a class of header_background_foto
    var $headerBackground = $('.header_background_foto');
    
    // set the width of the element to the current width of the device
    $headerBackground.css('width', $(window).width() + 'px');
    
    // add an event listener to the window object for the 'resize' event
    $(window).on('resize', function() {
      // update the width of the element to the current width of the device when the window is resized
      $headerBackground.css('width', $(window).width() + 'px');
    });
  });

  container = document.getElementById("container");

var containerRect = container.getBoundingClientRect();

var topPadding = containerRect.top;
var bottomPadding = window.innerHeight - containerRect.bottom;
var leftPadding = containerRect.left;
var rightPadding = window.innerWidth - containerRect.right;

console.log("Top padding: " + topPadding + "px");
console.log("Bottom padding: " + bottomPadding + "px");
console.log("Left padding: " + leftPadding + "px");
console.log("Right padding: " + rightPadding + "px");

var leftPadding = containerRect.left;

var headerBackground = document.querySelector(".header_background_foto");
header_background_foto = "-" + leftPadding + "px";

/*
var headerBackground = document.querySelector(".container");
var headerBackgroundWidth = headerBackground.offsetWidth;
var deviceWidth = window.screen.widthvar || window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
console.log( + headerBackground + "px");
var width = headerBackground.offsetWidth;
console.log(typeof(headerBackground))
console.log( + deviceWidth + "px");

var leftPadding = width - deviceWidth;
console.log(leftPadding+ "px");
var leftPadding = leftPadding / 2;
headerBackground.style.marginLeft = leftPadding+ "px";

document.querySelector('.header_background_foto').style.marginLeft = (leftPadding+ "px");
*/














/*
$(window).on('load resize',function(){
  var padding = 15;
  var page_width = $(document).outerWidth();
  var container_width = $('header .container').outerWidth();
  var width_add = (page_width - container_width) / 2 + padding;
  $('.full_width').css('width','calc(100% + '+width_add+')');
})

function calcFunction() {
  var padding = 15;
  var page_width = $(document).outerWidth();
  var container_width = $('header .container').outerWidth();
  var width_add = (page_width - container_width) / 2 + padding;
  $('.full_width').css('width','calc(100% + '+width_add+')');
}

window.addEventListener("resize",calcFunction);
window.addEventListener("load",calcFunction);
*/
